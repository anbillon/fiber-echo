// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package engine

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/anbillon/fiber"
)

type globalMiddleware struct {
	engine     fiber.ServerEngine
	middleware []echo.MiddlewareFunc
}

func init() {
	fiber.Container().WireWithOption(newGlobalMiddleware,
		fiber.Default(make([]echo.MiddlewareFunc, 0)))
}

// newGlobalMiddleware creates a new instance of initializer to initialize
// global middlewares used in echo server engine.
func newGlobalMiddleware(middleware []echo.MiddlewareFunc,
	ctx *fiber.AppContext) *globalMiddleware {
	return &globalMiddleware{
		engine:     ctx.Engine,
		middleware: middleware,
	}
}

func (gm *globalMiddleware) OnAppStart() {
	engine := gm.engine.(*EchoEngine)
	if gm.middleware == nil || len(gm.middleware) == 0 {
		return
	}

	engine.Echo.Use(gm.middleware...)
}
