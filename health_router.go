// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package engine

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/anbillon/fiber"
)

// A definition for health routersFunc.
type healthRouter struct {
}

func init() {
	fiber.Container().Wire(newHealthRouter)
}

// newHealthRouter creates a new instance of health router.
func newHealthRouter() Router {
	return &healthRouter{}
}

func (r *healthRouter) Register(e *echo.Echo) {
	e.GET("/health", r.check)
}

// Just to tell that this service is alive.
func (r *healthRouter) check(ctx echo.Context) error {
	return ctx.String(http.StatusOK, "OK")
}
