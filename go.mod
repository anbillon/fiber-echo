module gitlab.com/anbillon/fiber-echo

go 1.12

require (
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/labstack/echo/v4 v4.1.5
	github.com/labstack/gommon v0.2.8
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/rs/zerolog v1.14.3
	gitlab.com/anbillon/fiber v0.0.3
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.0
)
