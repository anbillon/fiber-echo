// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package engine

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/anbillon/fiber"
)

// ErrorHandler defines a error handler for echo.
type ErrorHandler interface {
	// HandleError handles the given error and returns the result.
	HandleError(error, echo.Context) interface{}
}

type errHandlerRegister struct {
	engine  fiber.ServerEngine
	handler ErrorHandler
}

type emptyHandler struct {
}

func newEmptyHandler() ErrorHandler {
	return ErrorHandler(&emptyHandler{})
}

func (*emptyHandler) HandleError(error, echo.Context) interface{} {
	return nil
}

func init() {
	fiber.Container().WireWithOption(newErrHandlerRegister,
		fiber.Default(newEmptyHandler()))
}

func newErrHandlerRegister(handlers ErrorHandler,
	ctx *fiber.AppContext) *errHandlerRegister {
	return &errHandlerRegister{
		engine:  ctx.Engine,
		handler: handlers,
	}
}

func (r *errHandlerRegister) OnAppStart() {
	engine := r.engine.(*EchoEngine)
	e := engine.Echo
	engine.Echo.HTTPErrorHandler = r.wrapErrHandler(e.HTTPErrorHandler)
}

func (r *errHandlerRegister) wrapErrHandler(
	originErrHandler echo.HTTPErrorHandler) echo.HTTPErrorHandler {

	return func(err error, c echo.Context) {
		e := c.Echo()
		result := r.handler.HandleError(err, c)
		if result != nil {
			if c.Response().Committed {
				return
			}
			code := c.Response().Status
			if code == 0 {
				code = http.StatusInternalServerError
			}
			err := c.JSON(code, result)
			if err != nil {
				e.Logger.Error(err)
			}
		} else {
			originErrHandler(err, c)
		}
	}
}
