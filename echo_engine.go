// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package engine

import (
	"github.com/labstack/echo/v4"
)

// An implementation of fiber.ServerEngine with echo.
type EchoEngine struct {
	Echo *echo.Echo
}

// Router defines echo router interface.
type Router interface {
	// Register registers this router into echo.
	Register(e *echo.Echo)
}

// NewEchoEngine creates a new instance of echo engine.
func NewEchoEngine() *EchoEngine {
	e := echo.New()
	e.Logger = newEchoZeroLogger("fiber")
	e.Use(loggerMiddleware())
	e.Use(recoverMiddleware())

	return &EchoEngine{
		Echo: e,
	}
}

func (e *EchoEngine) Name() string {
	return "echo"
}

func (e *EchoEngine) Start(address string) error {
	echoValidator := newEchoValidator()
	e.Echo.Validator = echoValidator
	e.Echo.HideBanner = true
	e.Echo.HidePort = true

	return e.Echo.Start(address)
}
