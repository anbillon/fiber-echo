// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package engine

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"

	elog "github.com/labstack/gommon/log"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var (
	zeroLvlToEchoLvl = map[zerolog.Level]elog.Lvl{
		zerolog.DebugLevel: elog.DEBUG,
		zerolog.InfoLevel:  elog.INFO,
		zerolog.WarnLevel:  elog.WARN,
		zerolog.ErrorLevel: elog.ERROR,
		zerolog.NoLevel:    elog.OFF,
	}

	echoLvlToZerolvl = map[elog.Lvl]zerolog.Level{
		elog.DEBUG: zerolog.DebugLevel,
		elog.INFO:  zerolog.InfoLevel,
		elog.WARN:  zerolog.WarnLevel,
		elog.ERROR: zerolog.ErrorLevel,
		elog.OFF:   zerolog.NoLevel,
	}
)

// An adapter of zerolog to implement Echo.Logger.
type EchoZeroLogger struct {
	zeroLogger zerolog.Logger
	out        io.Writer
	prefix     string
	level      zerolog.Level
}

func newEchoZeroLogger(prefix string) *EchoZeroLogger {
	return &EchoZeroLogger{
		zeroLogger: log.Logger,
		out:        os.Stdout,
		prefix:     prefix,
		level:      zerolog.InfoLevel,
	}
}

func (l *EchoZeroLogger) Output() io.Writer {
	return l.out
}

func (l *EchoZeroLogger) SetOutput(w io.Writer) {
	l.zeroLogger = (*l).zeroLogger.Output(w)
	l.out = w
}

func (l *EchoZeroLogger) Prefix() string {
	return l.prefix
}

func (l *EchoZeroLogger) SetPrefix(p string) {
	l.prefix = p
}

func (l *EchoZeroLogger) Level() elog.Lvl {
	return zeroLvlToEchoLvl[l.level]
}

func (l *EchoZeroLogger) SetLevel(v elog.Lvl) {
	l.level = echoLvlToZerolvl[v]
}

func (*EchoZeroLogger) SetHeader(h string) {
	/* ignore */
}

func (l *EchoZeroLogger) Print(i ...interface{}) {
	zl := l.logWithFields()
	zl.WithLevel(zerolog.NoLevel).Str("level", "-").Msg(fmt.Sprint(i...))
}

func (l *EchoZeroLogger) Printf(format string, args ...interface{}) {
	zl := l.logWithFields()
	zl.WithLevel(zerolog.NoLevel).Str("level", "-").Msgf(format, fmt.Sprint(args...))
}

func (l *EchoZeroLogger) Printj(j elog.JSON) {
	zl := l.logWithFields()
	for k, v := range j {
		j, _ := json.Marshal(v)
		zl = zl.With().RawJSON(k, j).Logger()
	}
	zl.WithLevel(zerolog.NoLevel).Str("level", "-").Msg("")
}

func (l *EchoZeroLogger) Debug(i ...interface{}) {
	zl := l.logWithFields()
	zl.Debug().Msg(fmt.Sprint(i...))
}

func (l *EchoZeroLogger) Debugf(format string, args ...interface{}) {
	zl := l.logWithFields()
	zl.Debug().Msgf(format, args...)
}

func (l *EchoZeroLogger) Debugj(j elog.JSON) {
	l.logJson(j, zerolog.DebugLevel)
}

func (l *EchoZeroLogger) Info(i ...interface{}) {
	zl := l.logWithFields()
	zl.Info().Msg(fmt.Sprint(i...))
}

func (l *EchoZeroLogger) Infof(format string, args ...interface{}) {
	zl := l.logWithFields()
	zl.Info().Msgf(format, args...)
}

func (l *EchoZeroLogger) Infoj(j elog.JSON) {
	l.logJson(j, zerolog.InfoLevel)
}

func (l *EchoZeroLogger) Warn(i ...interface{}) {
	zl := l.logWithFields()
	zl.Warn().Msg(fmt.Sprint(i...))
}

func (l *EchoZeroLogger) Warnf(format string, args ...interface{}) {
	zl := l.logWithFields()
	zl.Warn().Msgf(format, args...)
}

func (l *EchoZeroLogger) Warnj(j elog.JSON) {
	l.logJson(j, zerolog.WarnLevel)
}

func (l *EchoZeroLogger) Error(i ...interface{}) {
	zl := l.logWithFields()
	zl.Error().Msg(fmt.Sprint(i...))
}

func (l *EchoZeroLogger) Errorf(format string, args ...interface{}) {
	zl := l.logWithFields()
	zl.Error().Msgf(format, args...)
}

func (l *EchoZeroLogger) Errorj(j elog.JSON) {
	l.logJson(j, zerolog.ErrorLevel)
}

func (l *EchoZeroLogger) Fatal(i ...interface{}) {
	zl := l.logWithFields()
	zl.Fatal().Msg(fmt.Sprint(i...))
}

func (l *EchoZeroLogger) Fatalj(j elog.JSON) {
	l.logJson(j, zerolog.FatalLevel)
}

func (l *EchoZeroLogger) Fatalf(format string, args ...interface{}) {
	zl := l.logWithFields()
	zl.Fatal().Msgf(format, args...)
}

func (l *EchoZeroLogger) Panic(i ...interface{}) {
	zl := l.logWithFields()
	zl.Panic().Msg(fmt.Sprint(i...))
}

func (l *EchoZeroLogger) Panicj(j elog.JSON) {
	l.logJson(j, zerolog.PanicLevel)
}

func (l *EchoZeroLogger) Panicf(format string, args ...interface{}) {
	zl := l.logWithFields()
	zl.Panic().Msgf(format, args...)
}

func (l *EchoZeroLogger) logWithFields() zerolog.Logger {
	zl := l.zeroLogger
	_, f, no, ok := runtime.Caller(2)
	if ok {
		f = filepath.Base(f)
		zl = l.zeroLogger.With().Str("file", f).Int("line", no).Logger()
	}

	return zl
}

func (l *EchoZeroLogger) logJson(j elog.JSON, level zerolog.Level) {
	zl := l.logWithFields()
	for k, v := range j {
		j, _ := json.Marshal(v)
		zl = zl.With().RawJSON(k, j).Logger()
	}
	zl.WithLevel(level).Msg("")
}
