// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package engine

import (
	"gopkg.in/go-playground/validator.v9"
)

type echoValidator struct {
	validator *validator.Validate
}

func newEchoValidator() *echoValidator {
	return &echoValidator{
		validator: validator.New(),
	}
}

func (v *echoValidator) Validate(i interface{}) error {
	return v.validator.Struct(i)
}
