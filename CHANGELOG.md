# 0.0.2
* Add error handler.
* Add `Default` wire option.

# 0.0.1
* First implementation for fiber `ServerEngine` with `echo`.
